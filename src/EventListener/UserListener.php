<?php


namespace App\EventListener;



use App\Entity\User;
use Doctrine\ORM\Event\PreUpdateEventArgs;

class UserListener
{

    public function preUpdate(PreUpdateEventArgs $eventArgs)
    {
        $entity = $eventArgs->getObject();
        if(!$entity instanceof User){
            return;
        }
        /*
         * Verifie si les champs last_login et updated_at ne sont pas ceux qui changent sinon on update la date
         */
        if ($eventArgs->hasChangedField('lastLoginAt') || $eventArgs->hasChangedField('updatedAt') ){
            return;
        }

        $em = $eventArgs->getObjectManager();
        $entity->setUpdatedAt(new \DateTime());
        $em->persist($entity);

    }
}
