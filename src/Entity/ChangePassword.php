<?php

namespace App\Entity;

use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ChangePassword Used For change user password
 * @package App\Entity
 */
class ChangePassword
{
    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @UserPassword()
     */
    private $oldPassword;

    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @Assert\Length(min=8)
     * @Assert\Expression(
     *     expression="this.validatePassword()",
     *     message="New password must not be similar to the old one")
     */
    private $newPassword;

    /**
     * @return mixed
     */
    public function getOldPassword()
    {
        return $this->oldPassword;
    }

    /**
     * @param mixed $oldPassword
     * @return ChangePassword
     */
    public function setOldPassword($oldPassword)
    {
        $this->oldPassword = $oldPassword;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNewPassword()
    {
        return $this->newPassword;
    }

    /**
     * @param mixed $newPassword
     * @return ChangePassword
     */
    public function setNewPassword($newPassword)
    {
        $this->newPassword = $newPassword;

        return $this;
    }

    public function validatePassword()
    {
        return !(strtolower($this->oldPassword) === strtolower($this->newPassword));
    }
}
